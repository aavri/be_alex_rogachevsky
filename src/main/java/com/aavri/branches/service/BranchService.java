package com.aavri.branches.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The service
 */
@Service
public class BranchService {
    @Autowired
    private RestTemplate halifaxService;

    @Value("${halifax.url}")
    private String url = "x";

    @Value("${halifax.accept}")
    private String accept = "x";

    private List<BranchData> cache = new ArrayList<>();
    private Set<String> cachedIds = new HashSet<>();
    private Date lastLoadedAt = null;


    public List<BranchData> getBranches(String city) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));
        headers.set("accept", accept);

        if (lastLoadedAt != null)
            headers.set("if-modified-since", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'").format(lastLoadedAt));
        lastLoadedAt = new Date();

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        try {
            ResponseEntity<String> response = halifaxService.exchange(url, HttpMethod.GET, entity, String.class);

            if (response.getBody() != null)
                // Field names starting with capital letters throw off automatic JSON parsing, so doing it manually: less typing anyway
                for (JsonNode data : new ObjectMapper().readTree(response.getBody()).get("data"))
                    for (JsonNode brand : data.get("Brand")) {
                        String brandName = brand.get("BrandName").asText();

                        for (JsonNode branch : brand.get("Branch")) {
                            String id = branch.get("Identification").asText();
                            if (!cachedIds.contains(id)) { // "modified since" doesn't work, hence this check
                                cachedIds.add(id);

                                JsonNode address = branch.get("PostalAddress");
                                JsonNode location = address.get("GeoLocation").get("GeographicCoordinates");

                                JsonNode addrCityJson = address.get("TownName");
                                String addrCity = addrCityJson == null ? "" : addrCityJson.asText();

                                JsonNode subDivJson = address.get("CountrySubDivision");
                                String subDiv = subDivJson == null || !subDivJson.isArray() || !subDivJson.iterator().hasNext() ? "" :
                                    subDivJson.iterator().next().asText();

                                cache.add(new BranchData(
                                    brandName + " / " + branch.get("Name").asText(),
                                    Float.parseFloat(location.get("Latitude").asText()),
                                    Float.parseFloat(location.get("Longitude").asText()),
                                    address.get("AddressLine").iterator().next().asText(),
                                    addrCity,
                                    subDiv,
                                    address.get("Country").asText(),
                                    address.get("PostCode").asText()));
                            }
                        }
                    }
        } catch (RestClientException | IOException e) {
            return null;
        }

        if (city == null)
            return cache;

        String lcCity = city.toLowerCase().trim();
        return cache.stream().filter(record -> record.getCity().toLowerCase().contains(lcCity)).collect(Collectors.toList());
    }
}

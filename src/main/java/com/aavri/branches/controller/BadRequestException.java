package com.aavri.branches.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Bad Request (400) Exception
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid request parameters")
public class BadRequestException extends RuntimeException {
}

package com.aavri.branches.controller;

import com.aavri.branches.service.BranchData;
import com.aavri.branches.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * Simple REST controller
 */
@RestController
public class ApiController {
    @Autowired
    private BranchService service;

    public static class Response {
        private List<BranchData> data;

        public Response() {
        }

        public Response(List<BranchData> data) {
            this.data = data;
        }

        public List<BranchData> getData() {
            return data;
        }

        public void setData(List<BranchData> data) {
            this.data = data;
        }
    }

    @RequestMapping(value = "/branches", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response branches(@RequestParam(required = false) String city) {
        List<BranchData> data = service.getBranches(city);
        if (data == null)
            throw new BadRequestException();
        return new Response(data);
    }
}

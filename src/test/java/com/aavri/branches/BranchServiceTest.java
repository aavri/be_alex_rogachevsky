package com.aavri.branches;

import com.aavri.branches.service.BranchService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class BranchServiceTest {
    @Mock
    private RestTemplate halifaxService;

    @InjectMocks
    private BranchService service;

    @Before
    public void setup() {
        when(halifaxService.exchange(anyString(), eq(HttpMethod.GET), isA(HttpEntity.class), eq(String.class))).
            thenReturn(new ResponseEntity<String>("{\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"Brand\": [\n" +
                "        {\n" +
                "          \"BrandName\": \"Halifax\",\n" +
                "          \"Branch\": [\n" +
                "            {\n" +
                "              \"Identification\": \"11863000\",\n" +
                "              \"Name\": \"TADCASTER\",\n" +
                "              \"PostalAddress\": {\n" +
                "                \"AddressLine\": [\n" +
                "                  \"HALIFAX BRANCH 24 BRIDGE STREET\"\n" +
                "                ],\n" +
                "                \"TownName\": \"TADCASTER\",\n" +
                "                \"CountrySubDivision\": [\n" +
                "                  \"NORTH YORKSHIRE\"\n" +
                "                ],\n" +
                "                \"Country\": \"GB\",\n" +
                "                \"PostCode\": \"LS24 9AL\",\n" +
                "                \"GeoLocation\": {\n" +
                "                  \"GeographicCoordinates\": {\n" +
                "                    \"Latitude\": \"53.884262\",\n" +
                "                    \"Longitude\": \"-1.261096\"\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            }]\n" +
                "        }]\n" +
                "    }]\n" +
                "}",
                HttpStatus.ACCEPTED));
    }

    @Test
    public void whenCitySpecified_thenSuccess() {
        Assert.assertNotNull(service.getBranches("bridge"));
    }

    @Test
    public void whenNoCitySpecified_thenSuccess() {
        Assert.assertNotNull(service.getBranches(null));
    }
}

Building and running:
~~~~~~~~~~~~~~~~~~~~~
Prerequisites: latest Java (11) and latest Gradle, both in the PATH
cd <project-dir> (where build.gradle is)
$ gradle build
$ java -jar build/libs/branches.jar
http://localhost:8080/branches
http://localhost:8080/branches?city=abc


Typical prod features skipped due to the reduced exercise scope and time considerations:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Robust API design with a GraphQL "schema" (prefered over OpenAPI given a choice).
HATEOAS links in the response (not drilling down to the branch anyeway).
Securing the API via Oauth2 (Spring Security impl, the simplest Password Credentials flow).
Cloud deployment (AWS) with CI (Jenkins).
Zipkin support.
Clustering support: e.g. distributed cache via a memory grid like Redis or Hazelcast. The last update date should be stored there as well.